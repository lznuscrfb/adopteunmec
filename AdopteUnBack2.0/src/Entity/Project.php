<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Profil", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $profilId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $snapshot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProfilId(): ?Profil
    {
        return $this->profilId;
    }

    public function setProfilId(?Profil $profilId): self
    {
        $this->profilId = $profilId;

        return $this;
    }

    public function getSnapshot(): ?string
    {
        return $this->snapshot;
    }

    public function setSnapshot(string $snapshot): self
    {
        $this->snapshot = $snapshot;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'snapshot' => $this->snapshot,
            'name' => $this->name,
            'link' => $this->link,
            'description' => $this->description,
        );
    }
}
