<?php

namespace App\Controller;


use App\Entity\User;
use App\Entity\Badge;
use App\Entity\Profil;
use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
/**
 * @Route("/api/v1")
 */
class DefaultController extends AbstractController
{
    
    /**
     *  @Route("/mdp/{id}", name="mdp", methods={"POST"})
     */
    public function createMdp(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
       
        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->find($request->get('id'));
        $user->setPassword($passwordEncoder->encodePassword($user, $request->get('id')));
        $pass = $user->getPassword();
        return new JsonResponse(["message" => $pass]);
    }

    /**
     * @Route("/badges", name="get_badges", methods={"GET"})
     */
    public function getBadgesAction(Request $request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $badges = $this->getDoctrine()->getRepository(Badge::class)->findAll();

        $serialisedBagdes = [];

        for($i = 0; $i < count($badges); $i++){
            $serialisedBagdes[$i] = Array(
                "id" => $badges[$i]->getId(),
                "name" => $badges[$i]->getName(),
                "img" => $badges[$i]->getImg()
            );
        }
        $response = new JsonResponse([
            'status' => 200,
            'response' => $serialisedBagdes
        ]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/badges/{idBadge}", name="get_badge", methods={"GET"})
     */
    public function getBadgeAction(Request $request, $idBadge)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $badges = json_decode($serializer->serialize($this->getDoctrine()->getRepository(Badge::class)->findOneById($idBadge),'json'));
        $response = new JsonResponse([
            'status' => 200,
            'response' => $badges
        ]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/profils", name="get_profiles", methods={"GET"})
     */
    public function getProfilesAction(Request $request)
    {

        $profils = $this->getDoctrine()->getRepository(Profil::class)->findAll();

        $profilsJson = array();

        foreach ($profils as $key => $profil) {
            $profilsJson[$key] = $this->serializeProfil($profil);
        }

        shuffle($profilsJson);

        $response = new JsonResponse([
            'status' => 200,
            'response' => $profilsJson
        ]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/profils/{idProfil}", name="get_profil", methods={"GET"})
     */
    public function getProfilAction(Request $request, $idProfil)
    {
        $profil = $this->getDoctrine()->getRepository(Profil::class)->findOneById($idProfil);

        $profilJson = $this->serializeProfil($profil);

        if(isset($profil)){
            $response =  new JsonResponse([
                'status' => 200,
                'response' => $profilJson
            ]);
        }
        else{
            $response =  new JsonResponse([
                'status' => 404,
                'response' => 'Profil not found.'
            ]);
        }

        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    public function serializeProfil($profil)
    {
        $profilJson = $profil->jsonSerialize();
        $profilJson['badges'] = null;
        foreach($profil->getProfilBadges() as $keyBadge => $profilBadge) {
            $profilJson['badges'][$keyBadge]['id'] = $profilBadge->getBadgeId()->getId();
            $profilJson['badges'][$keyBadge]['enable'] = $profilBadge->getEnable();
        }
        $profilJson['comments'] = array();
        foreach($profil->getComments() as $keyComment => $comment) {
            $profilJson['comments'][$keyComment] = $comment->jsonSerialize();
        }
        $profilJson['projects'] = array();
        foreach($profil->getProjects() as $keyProject => $project) {
            $profilJson['projects'][$keyProject] = $project->jsonSerialize();
        }

        return $profilJson;
    }

    /**
     * @Route("/profils/{idProfil}/images/picture", methods={"GET"})
     */
    public function imageAction(Request $request, $idProfil) {
        $path = $parameterInterface->get('kernel.project_dir').'/public/asset/profils/pictures/'.$idProfil.'.png';
        return new BinaryFileResponse($path);
    }

    /**
     * @Route("/badges/{idBadge}/image", methods={"GET"})
     */
    public function badgeImageAction(ParameterBagInterface $parameterInterface, Request $request, $idBadge) {
        $badge = $this->getDoctrine()->getRepository(Badge::class)->findOneById($idBadge);
        $idImage = $badge->getImg();
        $path = $parameterInterface->get('kernel.project_dir').'/public/asset/badges/'.$idImage;
        return new BinaryFileResponse($path);
    }
    /**
     * @Route("/filter", methods={"GET"})
     */
    public function ApplyFilter(Request $request){
        $filters = json_decode($request->getContent())->selectedBadge;
        $profils = $this->getDoctrine()->getRepository(Profil::class)->findAll();
        
        foreach($profils as $profil){

            $profilBadges = $profil->getProfilBadges()->toArray();

            $count = count($profilBadges);
            $profil->setScore(count(array_filter($profilBadges, function($elem) use($filters){
                return in_array($elem->getBadgeId()->getId(),$filters);
            })));
        }

        usort($profils,function($a,$b)
        {
            if ($a->getScore()==$b->getScore()) return 0;
            return ($a->getScore()<$b->getScore())?-1:1;
        });

        $profilsJson = array();

        foreach ($profils as $key => $profil) {
            $profilsJson[$key] = $this->serializeProfil($profil);
        }


        $response = new JsonResponse([
            'status' => 200,
            'response' => $profilsJson
        ]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }


    /**
     * @Route("/setcomment", name="setComment", methods={"POST"})
     */
    public function setCommentAction(Request $request, EntityManagerInterface $em)
    {
        try {
            $frontComment = $request->request->all();
            $comment = new Comment();
            $date = new \DateTime();
            $profil = $this->getDoctrine()->getRepository(Profil::class)->findById($frontComment["id"]);

            $comment->setPostedBy($frontComment["auteur"]);
            $comment->setText($frontComment["texte"]);
            $comment->setCreatedAt($date);
            $comment->setValidated(null);
            $comment->setProfil($profil);

            $em->persist($comment);
            $em->flush();

            return new Response("Commentaire bien ajouté", Response::HTTP_OK);
        } catch (\Exception $e){
            return new Response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
